// @flow
import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import {auth, provider, providerGoogle} from './components/firebase/index.js'
import Home from './components/home/index.js'
import Blank from './components/blank/index.js'
import List from './components/list/index.js'
import logo from './logo.svg';

import 'semantic-ui/dist/components/reset.css'
import 'semantic-ui/dist/semantic.min.css'
import 'semantic-ui/dist/components/icon.css'
import 'semantic-ui/dist/components/segment.css'
import 'semantic-ui/dist/components/button.css'
import 'semantic-ui/dist/components/menu.css'
import './App.css';

type Props = {
    /* */
}

type State = {
    user: string,
    activeLink: string,
    loginStatus: number
}

class App extends Component<Props, State> {
    state = {
        user: null,
        activeLink: 'Home',
        loginStatus: 0
    }

    async login() {
        const result = await auth().signInWithPopup(provider)
        this.setState({user: result.user})
    }

    async loginGoogle() {
        const result = await auth().signInWithPopup(providerGoogle)
        this.setState({user: result.user})
    }

    async logout() {
        await auth().signOut()
        this.setState({user: null})
    }

    async componentWillMount() {
        auth().onAuthStateChanged((user) => {
            if(user) this.setState({user})
        })
    }

    render() {
        const {user} = this.state

        const navLinks = [
            ['Home','/'],
            ['The list','/blank/undef']
        ]

        return (
            <div className='App'>
                <header className='App-header'>
                    <img src={logo} className='App-logo' alt='logo' />
                    <h1 className='App-title'>React Boilerplate</h1>
                </header>
                <Router>
                    <div>
                        <div className="ui secondary pointing menu">
                            {navLinks.map((navPoint) => {
                                return(
                                    <Link
                                        to={navPoint[1]}
                                        key={navPoint[1]}
                                    >
                                        <div
                                            onClick={() => this.setState({activeLink: navPoint[0]})}
                                            className={`item ${this.state.activeLink === navPoint[0] ? 'active' : ''}`}
                                        >
                                            {navPoint[0]}
                                        </div>
                                    </Link>
                                )
                            })}
                            <div className="right menu">
                                <div className="ui item">
                                {!user &&
                                    <div>
                                        <button
                                            className="ui tiny blue button"
                                            onClick={this.loginGoogle.bind(this)}
                                        >
                                            Login with Google
                                        </button>
                                        <button
                                            style={{display: 'none'}}
                                            className="ui tiny blue button"
                                            onClick={this.login.bind(this)}
                                        >
                                            Login with Facebook
                                        </button>
                                    </div>
                                }
                                {user &&
                                    <button
                                        className="ui red tiny button"
                                        onClick={this.logout.bind(this)}
                                    >
                                        Logout
                                    </button>
                                }
                                </div>
                            </div>
                        </div>
                        <div className="ui segment">
                            <Route
                                path="/blank/:count"
                                render={(props) => <Blank {...props} />}
                            />
                            <Route
                                path="/blank/"
                                render={(props) => <List {...props} />}
                            />
                            <Route
                                exact
                                path="/"
                                render={(state) => <Home user={this.state.user ? this.state.user.displayName : 'Stranger'} />}
                            />
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
