// @flow

import * as React from 'react';

import 'semantic-ui/dist/semantic.min.css'

type Props = {
    user: object
};

class Home extends React.Component<Props> {
    static defaultProps = {
        user: 'Stranger'
    };

    render () {
        return(
            <h3
                className="ui header"
            >
                You're now seeing the root of this website. Follow some of the sweet links or login if you like, {this.props.user}.
            </h3>
        )
    }
}

export default Home