// src/components/firebase/firebase.js
import firebase from 'firebase'
const config = {
    apiKey: "AIzaSyBJYiY3kdJeRuco9tWGsnpULZF5L-d0a7s",
    authDomain: "portfolio-1e40f.firebaseapp.com",
    databaseURL: "https://portfolio-1e40f.firebaseio.com",
    projectId: "portfolio-1e40f",
    storageBucket: "portfolio-1e40f.appspot.com",
    messagingSenderId: "595623368910"
}
firebase.initializeApp(config)

export const ref = firebase.database().ref()
export const auth = firebase.auth
export const provider = new firebase.auth.FacebookAuthProvider();
export const providerGoogle = new firebase.auth.GoogleAuthProvider();
export default firebase